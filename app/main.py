#!/usr/bin/env python3

from fastapi import FastAPI

from .routers import releases


app = FastAPI()
app.include_router(releases.router)


@app.get("/")
async def root():
    return {"message": "Welcome to RZ 2.0 API"}
