#!/usr/bin/env python3

from datetime import datetime
from fastapi import APIRouter
from pydantic import BaseModel
from typing import List


router = APIRouter()


class Release(BaseModel):
    name: str
    description: str
    date: datetime


fake_data = [
    Release(name="Test Release", description="No description", date=datetime.now()),
    Release(name="Another Release", description="Some more description", date=datetime.now()),
]


@router.get("/releases/", response_model=List[Release])
async def root() -> List[Release]:
    return fake_data
